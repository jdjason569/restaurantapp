import React, {useState, useEffect} from 'react';
import * as firebase from 'firebase';
import UserGuest from './UserGuest';
import UserLogged from './UserLogged';
import Loading from '../../components/Loading';

export default function Account() {
  const [login, setLogin] = useState(null); //nos permite fijar un valor en este caso null,  login = variable, setLogin e sla funcion que
  //modifica logiin
  useEffect(() => {
    //carga cuando se lanza el componente y nos permite ejecutarse cuando se e
    //specifique alguna variable en [] es decir cambie el estado
    firebase.auth().onAuthStateChanged(user => {
      !user ? setLogin(false) : setLogin(true);
    });
  }, []);
  if (login === null)
    return <Loading isVisible={true} text="Espera un momento..." />;

  return login ? <UserLogged /> : <UserGuest />;
}
