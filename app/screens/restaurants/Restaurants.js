import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import {firebaseApp} from '../../utils/firebase';
import firebase from 'firebase/app';

export default function Restaurants(props) {
  const {navigation} = props;
  const [user, setUser] = useState(null);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(userInfo => {
      setUser(userInfo);
    });
  }, []);

  return (
    <View style={styles.viewBody}>
      {user &&
        <Icon
          reverse
          containerStyle={styles.btnContainer}
          type="material-community"
          name="plus"
          color="#00a680"
          onPress={() => navigation.navigate('add-restaurant')}
        />}
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: '#fff',
  },
  btnContainer: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    shadowColor: 'black',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
  },
});
