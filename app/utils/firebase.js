import firebase from 'firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyDly0Eb3DOI57bKzTd9ZtN9Q7zAvIpxRzY',
  authDomain: 'restaurantes-8189a.firebaseapp.com',
  databaseURL: 'https://restaurantes-8189a.firebaseio.com',
  projectId: 'restaurantes-8189a',
  storageBucket: 'restaurantes-8189a.appspot.com',
  messagingSenderId: '573144356046',
  appId: '1:573144356046:web:38a11d6c3843a1f6442c9f',
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
