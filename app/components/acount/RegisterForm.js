import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Input, Icon, Button} from 'react-native-elements';
import {validateEmail} from '../../utils/validations';
import {size, isEmpty} from 'lodash';
import * as firebase from 'firebase';
import {useNavigation} from '@react-navigation/native';
import Loading from '../../components/Loading';

export default function RegisterForm(props) {
  const {toastRef} = props;
  const [formData, setformData] = useState(defaultValue());
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const onSubmit = () => {
    if (
      isEmpty(formData.email) ||
      isEmpty(formData.password) ||
      isEmpty(formData.repeatPassword)
    ) {
      toastRef.current.show('Todos los campos son obligatorios');
    } else if (formData.password !== formData.repeatPassword) {
      toastRef.current.show('Las contraseñas tienen que ser iguales');
    } else if (size(formData.password) < 6) {
      toastRef.current.show(
        'La contraseña tiene que tener al menos 6 caracteres'
      );
    } else {
      setLoading(true);
      firebase
        .auth()
        .createUserWithEmailAndPassword(formData.email, formData.password)
        .then(response => {
          setLoading(false);
          navigation.navigate('account');
        })
        .catch(err => {
          setLoading(false);
          console.log('Error -> ', err);
          toastRef.current.show('El email ya existe, ingrese uno diferente');
        });
    }
  };

  const onChange = (e, type) => {
    setformData({...formData, [type]: e.nativeEvent.text});
  };
  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Correo Electronico"
        containerStyle={styles.inputForm}
        onChange={e => onChange(e, 'email')}
      />
      <Input
        placeholder="Contraseña"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={true}
        onChange={e => onChange(e, 'password')}
      />
      <Input
        placeholder="Repita su contraseña"
        containerStyle={styles.inputForm}
        password={true}
        secureTextEntry={true}
        onChange={e => onChange(e, 'repeatPassword')}
      />
      <Button
        title="Unirse"
        onPress={onSubmit}
        containerStyle={styles.btnContainerRegister}
      />
      <Loading isVisible={loading} text="Creando cuenta" />
    </View>
  );
}

function defaultValue() {
  return {
    email: '',
    password: '',
    repeatPassword: '',
  };
}

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  inputForm: {
    width: '100%',
    marginTop: 20,
  },
  btnContainerRegister: {
    marginTop: 20,
    width: '100%',
  },
});
